
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class VnexpressTest {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "F:\\DataSection//chromedriver.exe");
		WebDriver driver = new ChromeDriver(); 
		driver.navigate().to("https://vnexpress.net/");
		Set<String> set = new HashSet<String>();
		
		WebElement xPath  = driver.findElement(By.xpath("/html/body/section[3]/section[3]"));
		List<WebElement> link  = xPath.findElements(By.tagName("a"));
		
		for(WebElement ele:link) {	
			String sLink = ele.getAttribute("href");
			set.add(sLink);
		}
		for(String string:set) {
			System.out.println(string);
		}		
		driver.close();
		
	}

}
