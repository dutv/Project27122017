import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class FirstTest {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "F:\\DataSection//chromedriver.exe");
		WebDriver driver = new ChromeDriver(); 
		driver.navigate().to("http://www.24h.com.vn/");
		//String s = driver.getCurrentUrl();
		//System.out.println(driver.getTitle());
		//System.out.println(driver.getPageSource());
		//System.out.println(driver.getWindowHandle());
		driver.manage().timeouts().pageLoadTimeout(10000, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Tin tức trong ngày")).click(); // return page tintuc
		driver.navigate().back(); // return home
		driver.navigate().forward(); // return tintuc
		driver.navigate().to("http://www.24h.com.vn/"); //return home
		
		
		//System.out.println(driver.findElement(By.id("google_osd_static_frame_7436831628972")));
		
		driver.close();
		
	}

}
