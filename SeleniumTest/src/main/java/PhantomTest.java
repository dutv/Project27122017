import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class PhantomTest {

	
	public static void main(String[] args) {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true);
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "F:\\DataSection\\PhanTom\\phantomjs-2.1.1-windows\\bin/phantomjs.exe");
		
		System.setProperty("webdriver.chrome.driver", "F:\\DataSection/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.navigate().to("http://www.24h.com.vn/");
		System.out.println(driver.getTitle());
	}
}
